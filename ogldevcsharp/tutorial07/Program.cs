﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of OGL dev Modern OpenGL Tutorials
// Tutorial 07 - Rotation transformation
// http://ogldev.atspace.co.uk/www/tutorial07/tutorial07.html
// http://ogldev.atspace.co.uk/ogldev-source.zip

using System;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using Common;

namespace tutorial07
{
	class Program : GameWindow
	{
		private int VBO;
		private int gWorldLocation;

		private const string pVSFileName = "shader.vert";
		private const string pFSFileName = "shader.frag";

		float Scale = 0.0f;
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.Clear(ClearBufferMask.ColorBufferBit);

			Scale += 0.001f;

			var World = new Matrix4();

			World.M11 = (float)Math.Cos(Scale); World.M12 = -(float)Math.Sin(Scale);	World.M13 = 0.0f; World.M14 = 0.0f;
			World.M21 = (float)Math.Sin(Scale); World.M22 = (float)Math.Cos(Scale);		World.M23 = 0.0f; World.M24 = 0.0f;
			World.M31 = 0.0f;					World.M32 = 0.0f;						World.M33 = 1.0f; World.M34 = 0.0f;
			World.M41 = 0.0f;					World.M42 = 0.0f;						World.M43 = 0.0f; World.M44 = 1.0f;

			GL.UniformMatrix4(gWorldLocation, true, ref World);

			GL.EnableVertexAttribArray(0);
			GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);

			GL.DisableVertexAttribArray(0);

			SwapBuffers();
		}

		protected override void OnUpdateFrame(FrameEventArgs e)
		{
			base.OnUpdateFrame(e);

			Scale += 0.001f;
		}

		private void CreateVertexBuffer()
		{
			var Vertices = new Vector3[3];
			Vertices[0] = new Vector3(-1.0f, -1.0f, 0.0f);
			Vertices[1] = new Vector3(1.0f, -1.0f, 0.0f);
			Vertices[2] = new Vector3(0.0f, 1.0f, 0.0f);

			GL.GenBuffers(1, out VBO);
			GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector3.SizeInBytes * Vertices.Length), Vertices, BufferUsageHint.StaticDraw);
		}

		void AddShader(int ShaderProgram, string pShaderText, ShaderType ShaderType)
		{
			int ShaderObj = GL.CreateShader(ShaderType);

			Trace.Assert(ShaderObj != 0, "Error creating shader type " + ShaderType + "\n");

			var p = new string[1];
			p[0] = pShaderText;
			var Lengths = new int[1];
			Lengths[0] = pShaderText.Length;
			GL.ShaderSource(ShaderObj, 1, p, Lengths);
			GL.CompileShader(ShaderObj);
			int success;
			GL.GetShader(ShaderObj, ShaderParameter.CompileStatus, out success);
			if (success == 0)
			{
				string InfoLog = "";
				GL.GetShaderInfoLog(ShaderObj, out InfoLog);
				Trace.Fail("Error compiling shader type " + ShaderType + ": '" + InfoLog + "'\n");
				Exit();
			}

			GL.AttachShader(ShaderProgram, ShaderObj);
		}

		void CompileShaders()
		{
			int ShaderProgram = GL.CreateProgram();

			Trace.Assert(ShaderProgram != 0, "Error creating shader program\n");

			string vs = "", fs = "";

			if (!ogldev_util.ReadFile(pVSFileName, ref vs))
			{
				Exit();
			}

			if (!ogldev_util.ReadFile(pFSFileName, ref fs))
			{
				Exit();
			}

			AddShader(ShaderProgram, vs, ShaderType.VertexShader);
			AddShader(ShaderProgram, fs, ShaderType.FragmentShader);

			int Success = 0;
			string ErrorLog;

			GL.LinkProgram(ShaderProgram);
			GL.GetProgram(ShaderProgram, GetProgramParameterName.LinkStatus, out Success);
			if (Success == 0)
			{
				GL.GetProgramInfoLog(ShaderProgram, out ErrorLog);
				Trace.Fail("Error linking shader program: '" + ErrorLog + "'\n");
				Exit();
			}

			GL.ValidateProgram(ShaderProgram);
			GL.GetProgram(ShaderProgram, GetProgramParameterName.ValidateStatus, out Success);
			if (Success == 0)
			{
				GL.GetProgramInfoLog(ShaderProgram, out ErrorLog);
				Trace.Fail("Invalid shader program: '" + ErrorLog + "'\n");
				Exit();
			}

			GL.UseProgram(ShaderProgram);

			gWorldLocation = GL.GetUniformLocation(ShaderProgram, "gWorld");
			Debug.Assert(gWorldLocation != 0xFFFFFFFF);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			Debug.WriteLine("GL Version: " + GL.GetString(StringName.Version) + "\n");

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);

			CreateVertexBuffer();

			CompileShaders();
		}


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Width = 1024;
				program.Height = 768;
				program.Location = new System.Drawing.Point(100, 100);
				program.Title = "Tutorial 07";
				program.Run();
			}
		}
	}
}
