﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of OGL dev Modern OpenGL Tutorials
// Tutorial 01 - Create a window
// http://ogldev.atspace.co.uk/www/tutorial01/tutorial01.html
// http://ogldev.atspace.co.uk/ogldev-source.zip

using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace tutorial01
{
	class Program : GameWindow
	{
		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.Clear(ClearBufferMask.ColorBufferBit);
			SwapBuffers();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Width = 1024;
				program.Height = 768;
				program.Location = new System.Drawing.Point(100, 100);
				program.Title = "Tutorial 01";
				program.Run();
			}
		}
	}
}
