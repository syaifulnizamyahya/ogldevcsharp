﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Common
{
	public static class ogldev_util
	{
		public static bool ReadFile(string pFileName, ref string outFile)
		{
			bool ret = false;

			try
			{
				using (var streamReader = new StreamReader(pFileName))
				{
					outFile = streamReader.ReadToEnd();
				}

				ret = true;
			}
			catch (Exception exception)
			{
				Trace.Fail(pFileName + ": unable to open file '" + exception.Message + "'");
			}

			return ret;
		}

	}
}
