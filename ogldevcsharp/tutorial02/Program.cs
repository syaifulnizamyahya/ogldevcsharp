﻿// Copyright 2015 Syaiful Nizam Yahya. All Rights Reserved.
// C# port of OGL dev Modern OpenGL Tutorials
// Tutorial 02 - Hello dot!
// http://ogldev.atspace.co.uk/www/tutorial02/tutorial02.html
// http://ogldev.atspace.co.uk/ogldev-source.zip

using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace tutorial02
{
	class Program : GameWindow
	{
		private int VBO;

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);

			GL.Clear(ClearBufferMask.ColorBufferBit);

			GL.EnableVertexAttribArray(0);
			GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);

			GL.DrawArrays(PrimitiveType.Points, 0, 1);

			GL.DisableVertexAttribArray(0);

			SwapBuffers();
		}

		private void CreateVertexBuffer()
		{
			var Vertices = new Vector3[1];
			Vertices[0] = new Vector3(0.0f, 0.0f, 0.0f);

			GL.GenBuffers(1, out VBO);
			GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector3.SizeInBytes * Vertices.Length), Vertices, BufferUsageHint.StaticDraw);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			GL.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);

			CreateVertexBuffer();
		}


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			using (var program = new Program())
			{
				program.Width = 1024;
				program.Height = 768;
				program.Location = new System.Drawing.Point(100, 100);
				program.Title = "Tutorial 02";
				program.Run();
			}
		}
	}
}
